import React, { Component } from "react";
import Head from "next/head";
import axios from "axios";
import Header from "../../../../partials/Header";

// import TutorialsList from "../partials/TutorialsList";
// import Newsletter from "../partials/Newsletter";
// import Footer from "../partials/Footer";
const primaryPoiApiUrl = `https://4nos3u1s2h.execute-api.ap-south-1.amazonaws.com/seoProd/seo`;
const secondaryPoiApiUrl = `https://4nos3u1s2h.execute-api.ap-south-1.amazonaws.com/seoProd/seopoi`;

class index extends Component {
  render() {
    console.log(this.props, "props");
    let { poiPrimaryData, poiSecondaryData } = this.props;
    return (
      <div className="flex flex-col min-h-screen overflow-hidden">
        {/*  Site header */}
        <Header />

        {/*  Page content */}
        <main className="flex-grow">
          {/*  Page sections */}
          {JSON.stringify(poiPrimaryData)}

          {JSON.stringify(poiSecondaryData)}
        </main>

        {/*  Site footer */}
        {/* <Footer /> */}
      </div>
    );
  }
}

// executed once for every request
export async function getServerSideProps(context) {
  // fetch latest products from the API
  // const products = await fetchLatestProducts({ limit: 6 });
  // return {
  //   props: { products },
  // };
  console.log(Object.keys(context), context, "context");
  let { query } = context;

  let props = {};
  await axios
    .post(primaryPoiApiUrl, { uuid: query.uuid, iterationNumber: 1 })
    .then((r) => {
      // console.log(r, "response");
      props.poiPrimaryData = r.data.data;
    })
    .catch((e) => {
      console.log("error", i, "primary");
    });
  await axios
    .post(secondaryPoiApiUrl, { uuid: query.uuid, iterationNumber: 1 })
    .then((r) => {
      // console.log(r, "response");
      props.poiSecondaryData = r.data.data;
    })
    .catch((e) => {
      console.log("error", i, "primary");
    });
  return { props: props };
}

export default index;
