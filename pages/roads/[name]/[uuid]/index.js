import React, { Component } from "react";
import Head from "next/head";
import axios from "axios";
const primaryPoiApiUrl = `https://4nos3u1s2h.execute-api.ap-south-1.amazonaws.com/seoProd/seo`;
const secondaryPoiApiUrl = `https://4nos3u1s2h.execute-api.ap-south-1.amazonaws.com/seoProd/seopoi`;
const roadsPoiApiUrl = `https://4nos3u1s2h.execute-api.ap-south-1.amazonaws.com/seoProd/seoroads`;

class index extends Component {
  render() {
    console.log(this.props, "props");
    let { poiPrimaryData, poiSecondaryData } = this.props;
    return (
      <>
        <Head>
          <title>{poiPrimaryData.name}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {JSON.stringify(poiPrimaryData)}

        <div style={{ height: 100 }} />
        {JSON.stringify(poiSecondaryData)}
      </>
    );
  }
}

// executed once for every request
export async function getServerSideProps(context) {
  // fetch latest products from the API
  // const products = await fetchLatestProducts({ limit: 6 });
  // return {
  //   props: { products },
  // };
  console.log(Object.keys(context), context, "context");
  let { query } = context;

  let props = {};
  await axios
    .post(primaryPoiApiUrl, { uuid: query.uuid, iterationNumber: 1 })
    .then((r) => {
      // console.log(r, "response");
      props.poiPrimaryData = r.data.data;
    })
    .catch((e) => {
      console.log("error", i, "primary");
    });
  await axios
    .post(secondaryPoiApiUrl, { uuid: query.uuid, iterationNumber: 1 })
    .then((r) => {
      // console.log(r, "response");
      props.poiSecondaryData = r.data.data;
    })
    .catch((e) => {
      console.log("error", i, "primary");
    });
  return { props: props };
}

export default index;
